//Les Variables :

var navRecherche = document.getElementById("recherche");
var blockTitre = document.getElementById('afficheTitres');
var blockAuteur = document.getElementById('afficheAuteurs');
var blockSerie = document.getElementById('afficheSeries');
var barreAuteur = document.getElementById("auteurs")
var barreTitre = document.getElementById("titres");
var barreSerie = document.getElementById("series");


//Les abonnements: 
navRecherche.addEventListener("keyup", afficheRecherche);
barreAuteur.addEventListener("click",derouleAuteur);
barreTitre.addEventListener("click",cacheTitre);
barreSerie.addEventListener("click",derouleSerie);

//Si on arrive d'une autre page :

window.onload = afficheRechercheIni()


//Les fonctions :

function afficheRechercheIni() {

    var urlClef = new URL(window.location.href);
    var valueIni = urlClef.searchParams.get("value");

    var blockTitre = document.getElementById('afficheTitres');
    var blockAuteur = document.getElementById('afficheAuteurs');
    var blockSerie = document.getElementById('afficheSeries');
    blockSerie.innerHTML = "";
    blockAuteur.innerHTML = "";
    blockTitre.innerHTML = "";
    rechTitre(valueIni);
    rechSerie(valueIni);
    rechAuteur(valueIni);
}

function afficheRecherche(key) {

    if (key.keyCode === 13 || key === 13) {
        var valuetemp = navRecherche.value;
        navRecherche.value="";
        blockSerie.innerHTML = "";
        blockAuteur.innerHTML = "";
        blockTitre.innerHTML = "";
        rechTitre(valuetemp);
        rechSerie(valuetemp);
        rechAuteur(valuetemp);
        cacheAuteur();
        cacheSerie();
        derouleTitre();
    }
}


function rechTitre(valeur) {

    var t =0;
    for (let i = 0; i < albums.size; i++) {
        var iString = i.toFixed(0);
        if (albums.get(iString)) {
            var alb = albums.get(iString);
            if (((clearAccent(alb.titre)).toLowerCase()).includes((clearAccent(valeur)).toLowerCase())) {
                afficheTitre(iString);
                t++;
            }
        }
    }
    barreTitre.innerHTML = "Par titre : "+t+" référence(s) trouvée(s)";

}



function afficheTitre(indice) {
    //Récupérer les données :
    var monAlbum = albums.get(indice);
    var nomAlbum = monAlbum.titre;
    nomAlbum = nomAlbum.replace(/'|!|\?|\.|"|:|\$/g, ""); //supprime les caractères interdits
    var idAlbum = monAlbum.idSerie;
    var maSerie = series.get(idAlbum).nom;
    maSerie = maSerie.replace(/'|!|\?|\.|"|:|\$/g, "");
    var idAuteur = monAlbum.idAuteur;
    var monAuteur = auteurs.get(idAuteur).nom;
    var monNumero = monAlbum.numero;
    var monPrix = new Intl.NumberFormat('fr-FR', { style: 'currency', currency: 'EUR' }).format(parseFloat(monAlbum.prix));
    //test si prix incorrect :
    if (isNaN(parseFloat(monAlbum.prix))) {
        monPrix = "Prix à définir";
    }

    var monImage = "./albumsMini/" + maSerie + "-" + monNumero + "-" + nomAlbum + ".jpg";



    var block = document.getElementById('afficheTitres');
    var div = document.createElement("div");
    div.setAttribute('class', 'speech');

    var span = document.createElement("span");
    div.addEventListener("click", function () { afficherDetailbis(monAlbum, nomAlbum, idAlbum, maSerie, monAuteur, monNumero, monImage, monPrix) });
    span.innerHTML = "<h3>" + monAlbum.titre + "</h3>";
    div.appendChild(span);
    block.appendChild(div);

}

function afficherDetailbis(monAlbum, nomAlbum, idAlbum, maSerie, monAuteur, monNumero, monImage, monPrix) {

    var divMere = document.getElementById("afficheTitres");
    var divModal = document.createElement('div');
    divModal.setAttribute('class', 'modaldetail');
    divModal.setAttribute('id', 'myModalBD')
    var divContent = document.createElement('div');
    divContent.setAttribute('class', 'modaldetail-content speech');
    var img = document.createElement("img");
    var span = document.createElement("span");
    img.src = monImage;
    span.innerHTML = "<h3>" + monAlbum.titre + "</h3><p>" + monAuteur + "</p><p>" + monPrix + "</p>" + "<input type='button' id='ajout'>";
    divContent.appendChild(img);
    divContent.appendChild(span);
    divModal.appendChild(divContent);
    divMere.appendChild(divModal);
    divModal.style.display = "block";
    window.onclick = function (event) {
        if (event.target == divModal) {
            divModal.remove();
        }
    }
    var bouton = document.getElementById("ajout")
    bouton.addEventListener("click", function () { putToCart(monAlbum) });
}


function rechSerie(valeur) {
    var t =0;
    for (let i = 0; i < series.size; i++) {
        var iString = i.toFixed(0);

        if (series.get(iString)) {
            var ser = series.get(iString);
            if (((clearAccent(ser.nom)).toLowerCase()).includes((clearAccent(valeur)).toLowerCase())) {
                var maSerie = series.get(iString);
                catSerie(maSerie, iString);
                t++;
            }
        }
    }
    barreSerie.innerHTML = "Par série : "+t+" référence(s) trouvée(s)";
}

function catSerie(serie, indice) {
    var nomSerie = serie.nom;


    var block = document.getElementById('afficheSeries');
    var div = document.createElement("div");
    div.setAttribute('class', 'speech');

    var span = document.createElement('span');
    span.innerHTML = "<h3>" + nomSerie + "</h3>";
    div.addEventListener("click", function () { afficherCatSerie(indice) })
    div.appendChild(span);
    block.appendChild(div);
}

function rechAuteur(valeur) {
    var t =0;
    for (let i = 0; i < auteurs.size; i++) {
        var iString = i.toFixed(0);

        if (auteurs.get(iString)) {
            var aut = auteurs.get(iString);
            if (((clearAccent(aut.nom)).toLowerCase()).includes((clearAccent(valeur)).toLowerCase())) {
                var monAuteur = auteurs.get(iString);
                carAuteur(monAuteur, iString);
                t++;
            }
        }
    }
    barreAuteur.innerHTML="Par auteur : "+t+" référence(s) trouvée(s)";

}

function carAuteur(auteur, indice) {
    var nomAuteur = auteur.nom;
    var block = document.getElementById('afficheAuteurs');
    var div = document.createElement("div");
    div.setAttribute('class', 'speech');

    var span = document.createElement('span');
    span.innerHTML = "<h3>" + nomAuteur + "</h3>";
    div.addEventListener("click", function () { afficherCatAuteur(indice) })
    div.appendChild(span);
    block.appendChild(div);
}

function afficherCatAuteur(indice) {

    var i = 1;
    do {
        var iString = i.toFixed(0);
        if (albums.get(iString)) {
            if (albums.get(iString).idAuteur == indice) {

                window.location.href = "MonAuteur.html?Auteur=" + indice
            }

        }
        i++;
    } while (i < 629)
}

function clearAccent(str) {
    var accent = [
        /[\300-\306]/g, /[\340-\346]/g, // A, a
        /[\310-\313]/g, /[\350-\353]/g, // E, e
        /[\314-\317]/g, /[\354-\357]/g, // I, i
        /[\322-\330]/g, /[\362-\370]/g, // O, o
        /[\331-\334]/g, /[\371-\374]/g, // U, u
        /[\321]/g, /[\361]/g, // N, n
        /[\307]/g, /[\347]/g, // C, c
    ];
    var noaccent = ['A', 'a', 'E', 'e', 'I', 'i', 'O', 'o', 'U', 'u', 'N', 'n', 'C', 'c'];

    for (let i = 0; i < str.length; i++) {
        str = str.replace(accent[i], noaccent[i]);
    }
    return str
}

function afficherCatSerie(indice) {

    var i = 1;
    do {
        var iString = i.toFixed(0);
        if (albums.get(iString)) {
            if (albums.get(iString).idSerie == indice) {

                window.location.href = "MaSerie.html?Serie=" + indice
            }

        }
        i++;
    } while (i < 629)
}

// barreSerie.addEventListener("click",derouleSerie);

function derouleSerie(){
    blockSerie.style.display="inline";
    barreSerie.removeEventListener("click",derouleSerie);
    barreSerie.addEventListener("click",cacheSerie);
}

function cacheSerie(){
    blockSerie.style.display="none"
    barreSerie.removeEventListener("click",cacheSerie);
    barreSerie.addEventListener("click",derouleSerie);
}


function derouleTitre(){
    blockTitre.style.display="inline";
    barreTitre.removeEventListener("click",derouleTitre);
    barreTitre.addEventListener("click",cacheTitre);
}

function cacheTitre(){
    blockTitre.style.display="none"
    barreTitre.removeEventListener("click",cacheTitre);
    barreTitre.addEventListener("click",derouleTitre);
}


function derouleAuteur(){

    blockAuteur.style.display="inline";
    barreAuteur.removeEventListener("click",derouleAuteur);
    barreAuteur.addEventListener("click",cacheAuteur);
}

function cacheAuteur(){
    blockAuteur.style.display="none"
    barreAuteur.removeEventListener("click",cacheAuteur);
    barreAuteur.addEventListener("click",derouleAuteur);
}