// Les variables
var nbPages = 0;
var pageActuelle = 1;
var elemParPage = 20;
var nbElemTotal = 0;
var cleUrl = new URL(window.location.href);
var cleSerie = cleUrl.searchParams.get("Serie");
var cleAuteur = cleUrl.searchParams.get("Auteur");

//thanks to cyberpunk  2077
//Définir si mobile ou pc :
var isMobile = (typeof isMobile === 'undefined') ? (/mobile/i.test(navigator.userAgent) && ('ontouchstart' in window || (window.DocumentTouch && document instanceof DocumentTouch))) : isMobile;

if (isMobile) {
    elemParPage = 10;
}

definirPage(pageActuelle, elemParPage)
//Définir si affichage : toute bd, par série, par auteur :
function definirPage(pageActuelle, elemParPage) {
    var cleUrl = new URL(window.location.href);
    //Recherche existence d'une clef dans l'URL :
    var cleSerie = cleUrl.searchParams.get("Serie");
    var cleAuteur = cleUrl.searchParams.get("Auteur");

    if (cleSerie != null) {
        nbElemTotal = compterNbAlbum();
        nbPages = Math.ceil(nbElemTotal / elemParPage);
        coverFlowCat(pageActuelle, nbElemTotal, elemParPage, nbPages, cleSerie);
    }
    else if (cleAuteur != null) {
        nbElemTotal = compterNbAlbum();
        nbPages = Math.ceil(nbElemTotal / elemParPage);
        coverFlowCat(pageActuelle, nbElemTotal, elemParPage, nbPages, cleAuteur);
    }
    else {
        nbElemTotal = parseInt(albums.size);
        nbPages = Math.ceil(nbElemTotal / elemParPage);
        coverFlow(pageActuelle, nbElemTotal, elemParPage, nbPages);
    }
}
//Afficher les éléments
function coverFlow(pageActuelle, nbElemTotal, elemParPage, nbPages) {
    var i = 1; //indice
    var j = 0;  //Compteur pour parcourir map
    do {
        var iString = i.toFixed(0);

        if (j < ((elemParPage * pageActuelle) - elemParPage)) {    //Si page != 1 :

            j++;
        }
        else if (albums.get(iString) && j < (nbElemTotal)) {
            var monAlbum = albums.get(iString);
            afficherElement(monAlbum);
            j++;
        }
        else if (j == nbElemTotal) {
            break;
        }
        i++;
    } while (j < (elemParPage * pageActuelle))   // Tant que j< nb élément souhaité
    navigate(pageActuelle, nbPages);
}

function coverFlowCat(pageActuelle, nbElemTotal, elemParPage, nbPages, indice) {
    var i = 1; // indice
    var j = 0; // Compteur pour arrêter si toute la map parcouru
    var t = 0; // Compteur albums affichés

    if (cleSerie != null) {
        do {
            var iString = i.toFixed(0);

            if (albums.get(iString)) {
                var monAlbum = albums.get(iString);
                if (monAlbum.idSerie == indice) {
                    if (t < ((elemParPage * pageActuelle) - elemParPage)) {
                        t++
                    }
                    else {
                        afficherElement(monAlbum);
                        t++
                    }
                }
                j++;
            }
            else if (j == albums.size) {
                break;
            }
            i++;
        } while (t < (elemParPage * pageActuelle))
    }
    else {
        do {
            var iString = i.toFixed(0);

            if (albums.get(iString)) {
                var monAlbum = albums.get(iString);

                if (monAlbum.idAuteur == indice) {
                    if (t < ((elemParPage * pageActuelle) - elemParPage)) {
                        t++
                    }
                    else {
                        afficherElement(monAlbum);
                        t++
                    }
                }
                j++;
            }
            else if (j == albums.size) {
                break;
            }
            i++;
        } while (t < (elemParPage * pageActuelle))
    }
    if (nbElemTotal > elemParPage) {
        navigateCat(nbPages);
    }
}

function afficherElement(monAlbum) {
    //Récupérer les données :
    var nomAlbum = monAlbum.titre;
    nomAlbum = nomAlbum.replace(/'|!|\?|\.|"|:|\$/g, ""); //supprime les caractères interdits
    var idAlbum = monAlbum.idSerie;
    var maSerie = series.get(idAlbum).nom;
    maSerie = maSerie.replace(/'|!|\?|\.|"|:|\$/g, "");
    var idAuteur = monAlbum.idAuteur;
    var monAuteur = auteurs.get(idAuteur).nom;
    var monNumero = monAlbum.numero;
    var monPrix = new Intl.NumberFormat('fr-FR', { style: 'currency', currency: 'EUR' }).format(parseFloat(monAlbum.prix));
    //test si prix incorrect :
    if (isNaN(parseFloat(monAlbum.prix))) {
        monPrix = "Prix à définir";
    }

    var monImage = "albumsMini/" + maSerie + "-" + monNumero + "-" + nomAlbum + ".jpg";
    //Afficher les images :
    var block = document.getElementById('afficheAlbums');
    var div = document.createElement("div");
    div.setAttribute('class', 'speech');

    var img = document.createElement("img");
    var span = document.createElement("span");
    img.src = monImage;
    img.id = monImage;
    div.addEventListener("click", function () { afficherDetail(monAlbum, nomAlbum, idAlbum, maSerie, monAuteur, monNumero, monImage, monPrix) });
    span.innerHTML = "<h3>" + monAlbum.titre + "</h3><p>" + monAuteur + "</p><p>" + monPrix + "</p>"
    div.appendChild(img);
    div.appendChild(span);
    block.appendChild(div);


}

//Afficher détails :

function afficherDetail(monAlbum, nomAlbum, idAlbum, maSerie, monAuteur, monNumero, monImage, monPrix) {

    var divMere = document.getElementById("afficheAlbums");
    var divModal = document.createElement('div');
    divModal.setAttribute('class', 'modaldetail');
    divModal.setAttribute('id', 'myModalBD')
    var divContent = document.createElement('div');
    divContent.setAttribute('class', 'modaldetail-content speech');
    var img = document.createElement("img");
    var span = document.createElement("span");
    var grdeImage = monImage.replace('albumsMini', 'albums');
    if (screen.width > 700 && screen.height > 700) {
        img.src = grdeImage;
    }
    else {
        img.src = monImage;
    }

    span.innerHTML = "<h3>" + monAlbum.titre + "</h3><p>" + monAuteur + "</p><p>" + monPrix + "</p>" + "<input type='button' id='ajout' value='Acheter'>";
    divContent.appendChild(img);
    divContent.appendChild(span);
    divModal.appendChild(divContent);
    divMere.appendChild(divModal);
    divModal.style.display = "block";
    window.onclick = function (event) {
        if (event.target == divModal) {
            divModal.remove();
        }
    }
    var bouton = document.getElementById("ajout")
    bouton.addEventListener("click", function () { putToCart(monAlbum) });
}

//Créer barre de navigation

function navigate(pageActuelle) {
    var divBarre = document.getElementById("barre");
    divBarre.innerHTML = '';
    var symbBtnDebut = " &laquo;"
    var symbBtnFin = " &raquo;"
    var symbBtnRecule = " &#10094;"
    var symbBtnAvance = " &#10095;"
    var symbNumerique = "";
    //Bouton pour revenir premiere page :
    let btnDeb = 1;
    creatBouton(divBarre, btnDeb, symbBtnDebut);
    //Choix des boutons à afficher
    if (pageActuelle < 4) {
        //Boutons numériques :
        for (let k = 1; k <= 5; k++) {
            creatBouton(divBarre, k, symbNumerique);
        }
        //Bouton avance de 5 pages :
        let btnAv = pageActuelle + 5;
        if (btnAv > nbPages) {
            btnAv = nbPages;
        }
        creatBouton(divBarre, btnAv, symbBtnAvance);
    }
    else if (pageActuelle > 3 && pageActuelle < (nbPages - 3)) {
        //Bouton recule de 5 pages :
        let btnRec = pageActuelle - 5;
        if (btnRec <= 0) {
            btnRec = 1;
        }
        creatBouton(divBarre, btnRec, symbBtnRecule);
        //Boutons numériques :
        for (let k = pageActuelle - 2; k <= pageActuelle + 2; k++) {
            creatBouton(divBarre, k, symbNumerique);
        }
        //Bouton avance de 5 pages :
        let btnAv = pageActuelle + 5;
        if (btnAv > nbPages) {
            btnAv = nbPages;
        }
        creatBouton(divBarre, btnAv, symbBtnAvance);
    }
    else {
        let btnRec = pageActuelle - 5;
        if (btnRec <= 0) {
            btnRec = 1;
        }
        creatBouton(divBarre, btnRec, symbBtnRecule);
        for (let k = (nbPages - 5); k <= nbPages; k++) {
            creatBouton(divBarre, k, symbNumerique);
        }
    }
    //Boutton aller page 27 :
    let bEnd = nbPages;
    creatBouton(divBarre, bEnd, symbBtnFin);
}

function navigateCat(nbPages) {
    var divBarre = document.getElementById("barre");
    divBarre.innerHTML = '';
    var symbNumerique = "";

    for (let k = 1; k <= nbPages; k++) {
        creatBouton(divBarre, k, symbNumerique);
    }
}


function nouvellePage(k) {
    if (pageActuelle != k) {
        pageActuelle = k;
        let divCover = document.getElementById('afficheAlbums');
        divCover.innerHTML = '';

        if (cleSerie != null) {
            coverFlowCat(pageActuelle, nbElemTotal, elemParPage, nbPages, cleSerie);
        }
        else if (cleAuteur != null) {
            coverFlowCat(pageActuelle, nbElemTotal, elemParPage, nbPages, cleAuteur);
        }
        else {
            coverFlow(pageActuelle, nbElemTotal, elemParPage, nbPages);
        }
    }

}

function creatBouton(divBarre, k, symb) {
    let btn = document.createElement('a');
    btn.addEventListener("click", function () { nouvellePage(k) });
    btn.setAttribute('href', '#');
    btn.setAttribute("class", "w3-button");

    if (symb == "") {
        let symbAjout = " " + k;
        btn.innerHTML = symbAjout;
    }
    else {
        btn.innerHTML = symb
    }
    divBarre.appendChild(btn);
}

function compterNbAlbum() {
    var i = 0;

    var nbAlbumtemp = 0;
    do {
        var albtemp = albums.get(i.toFixed(0));

        if (albtemp != undefined) {
            if (albtemp.idSerie == cleSerie) {
                nbAlbumtemp++
            }
        }
        i++
    } while (i < albums.size)
    return (nbAlbumtemp)
}



