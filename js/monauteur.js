//Afficher albums avec id Série = clef

//1 - Retrouver la clé :
var urlClef = new URL(window.location.href);

var clefauteur = urlClef.searchParams.get("Auteur");



//2 - Trouver albums par clé :

// Les variables
var nbPages = 0;
var pageActuelle = 1;
var albparpage = 10;
var nbAlbumtotal = 0;
//thanks to cyberpunk  2077
var isMobile = (typeof isMobile === 'undefined') ? (/mobile/i.test(navigator.userAgent) && ('ontouchstart' in window || (window.DocumentTouch && document instanceof DocumentTouch))) : isMobile;


//Définir nombres d'album total à afficher
nbAlbumtotal = compterNbAlbum();
console.log(nbAlbumtotal);


nbPages = Math.ceil(nbAlbumtotal / albparpage);

//Définir les éléments à afficher

coverFlow(pageActuelle, nbAlbumtotal, albparpage, nbPages);

//Afficher les éléments

function coverFlow(pageActuelle, nbAlbumtotal, albparpage, nbPages) {
    var i = 1; // indice
    var j = 0; // Compteur pour arrêter si toute la map parcouru
    var t = 0; // Compteur albums affichés
    do {
        var iString = i.toFixed(0);


        if (albums.get(iString)) {
            var monAlbum = albums.get(iString);


            if (monAlbum.idAuteur == clefauteur) {
                if (t < ((albparpage * pageActuelle) - albparpage)) {
                    t++

                }
                else {
                    afficherElement(monAlbum);
                    t++
                }

            }
            j++;

        }
        else if (j == albums.size) {
            break;
        }
        i++;
    } while (t < (albparpage * pageActuelle))
    if (nbAlbumtotal > albparpage) {
        navigate(pageActuelle, nbPages);
    }

}

function afficherElement(monAlbum) {
    //Récupérer les données :
    var nomAlbum = monAlbum.titre;
    nomAlbum = nomAlbum.replace(/'|!|\?|\.|"|:|\$/g, ""); //supprime les caractères interdits
    var idAlbum = monAlbum.idSerie;
    var maSerie = series.get(idAlbum).nom;
    maSerie = maSerie.replace(/'|!|\?|\.|"|:|\$/g, "");
    var idAuteur = monAlbum.idAuteur;
    var monAuteur = auteurs.get(idAuteur).nom;
    var monNumero = monAlbum.numero;
    var monPrix = new Intl.NumberFormat('fr-FR', { style: 'currency', currency: 'EUR' }).format(parseFloat(monAlbum.prix));
    //test si prix incorrect :
    if (isNaN(parseFloat(monAlbum.prix))) {
        monPrix = "Prix à définir";
    }

    var monImage = "./albumsMini/" + maSerie + "-" + monNumero + "-" + nomAlbum + ".jpg";
    //Afficher les images :
    var block = document.getElementById('afficheAlbums');
    var div = document.createElement("div");
    div.setAttribute('class', 'speech');

    var img = document.createElement("img");
    var span = document.createElement("span");
    img.src = monImage;
    img.id = monImage;
    div.addEventListener("click", function () { afficherDetail(monAlbum, nomAlbum, idAlbum, maSerie, monAuteur, monNumero, monImage, monPrix) });
    span.innerHTML = "<h3>" + monAlbum.titre + "</h3><p>" + monAuteur + "</p><p>" + monPrix + "</p>"
    div.appendChild(img);
    div.appendChild(span);
    block.appendChild(div);


}

//Afficher détails :

function afficherDetail(monAlbum, nomAlbum, idAlbum, maSerie, monAuteur, monNumero, monImage, monPrix) {

    var divMere = document.getElementById("afficheAlbums");
    var divModal = document.createElement('div');
    divModal.setAttribute('class', 'modaldetail');
    divModal.setAttribute('id', 'myModalBD')
    var divContent = document.createElement('div');
    divContent.setAttribute('class', 'modaldetail-content speech');
    var img = document.createElement("img");
    var span = document.createElement("span");
    var grdeImage = monImage.replace('albumsMini', 'albums');
    if (screen.width > 700 && screen.height > 700) {
        img.src = grdeImage;
    }
    else {
        img.src = monImage;
    }

    span.innerHTML = "<h3>" + monAlbum.titre + "</h3><p>" + monAuteur + "</p><p>" + monPrix + "</p>" + "<input type='button' id='ajout'>";
    divContent.appendChild(img);
    divContent.appendChild(span);
    divModal.appendChild(divContent);
    divMere.appendChild(divModal);
    divModal.style.display = "block";
    window.onclick = function (event) {
        if (event.target == divModal) {
            divModal.remove();
        }
    }
    var bouton = document.getElementById("ajout")
    bouton.addEventListener("click", function () { putToCart(monAlbum) });
}

//Créer barre de navigation
function navigate(pageActuelle, nbPages) {
    var divBarre = document.getElementById("barre");
    divBarre.innerHTML = '';
    var symbBtnDebut = " &laquo;"
    var symbBtnFin = " &raquo;"
    var symbBtnRecule = " &#10094;"
    var symbBtnAvance = " &#10095;"
    var symbNumerique = "";
    if (nbAlbumtotal == parseInt(albums.size)) {
        //Bouton pour revenir premiere page :
        let btnDeb = 1;
        creatBouton(divBarre, btnDeb, symbBtnDebut);
        //Choix des boutons à afficher
        if (pageActuelle < 4) {
            //Boutons numériques :
            for (let k = 1; k <= 5; k++) {
                creatBouton(divBarre, k, symbNumerique);
            }
            //Bouton avance de 5 pages :
            let btnAv = pageActuelle + 5;
            if (btnAv > nbPages) {
                btnAv = nbPages;
            }
            creatBouton(divBarre, btnAv, symbBtnAvance);
        }
        else if (pageActuelle > 3 && pageActuelle < (nbPages - 3)) {
            //Bouton recule de 5 pages :
            let btnRec = pageActuelle - 5;
            if (btnRec <= 0) {
                btnRec = 1;
            }
            creatBouton(divBarre, btnRec, symbBtnRecule);
            //Boutons numériques :
            for (let k = pageActuelle - 2; k <= pageActuelle + 2; k++) {
                creatBouton(divBarre, k, symbNumerique);
            }
            //Bouton avance de 5 pages :
            let btnAv = pageActuelle + 5;
            if (btnAv > nbPages) {
                btnAv = nbPages;
            }
            creatBouton(divBarre, btnAv, symbBtnAvance);
        }
        else {
            let btnRec = pageActuelle - 5;
            if (btnRec <= 0) {
                btnRec = 1;
            }
            creatBouton(divBarre, btnRec, symbBtnRecule);
            for (let k = (nbPages - 5); k <= nbPages; k++) {
                creatBouton(divBarre, k, symbNumerique);
            }
        }
        //Boutton aller page 27 :
        let bEnd = nbPages;
        creatBouton(divBarre, bEnd, symbBtnFin);
    }
    else {
        for (let k = 1; k <= nbPages; k++) {
            creatBouton(divBarre, k, symbNumerique);
        }
    }
}

function nouvellePage(k) {
    if (pageActuelle != k) {
        pageActuelle = k;
        let divCover = document.getElementById('afficheAlbums');
        divCover.innerHTML = '';
        coverFlow(pageActuelle, nbAlbumtotal, albparpage, nbPages);
    }

}

function creatBouton(divBarre, k, symb) {
    let btn = document.createElement('a');
    btn.addEventListener("click", function () { nouvellePage(k) });
    btn.setAttribute('href', '#');
    btn.setAttribute("class", "w3-button");

    if (symb == "") {
        let symbAjout = " " + k;
        btn.innerHTML = symbAjout;
    }
    else {
        btn.innerHTML = symb
    }
    divBarre.appendChild(btn);
}



function compterNbAlbum() {
    var i = 0;

    var nbAlbumtemp = 0;
    do {
        var albtemp = albums.get(i.toFixed(0));

        if (albtemp != undefined) {
            if (albtemp.idAuteur == clefauteur) {
                nbAlbumtemp++
            }
        }
        i++
    } while (i < albums.size)
    return (nbAlbumtemp)
}

