// Get the modal
var modal = document.getElementById("myModal");
// Get the button that opens the modal
var btn = document.getElementById("panier");
// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[ 0 ];
// When the user clicks on the button, open the modal
function display() {
    modal.style.display = "block";
}
btn.onclick = afficherPanier;

// When the user clicks on <span> (x), close the modal
span.onclick = function () {
    modal.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.addEventListener("click", function (event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
})

/*                      gestion panier                        */

var valId = 0;
var shopCart = [];
var cartShop = [];
var sum;

/**
 * ajoute un album au panier
 * @param {string} monAlbum 
 */
function putToCart(monAlbum) {

    var modalBD = document.getElementById("myModalBD");
    modalBD.remove();
    var nomAlbum = monAlbum.titre;
    nomAlbum = nomAlbum.replace(/'|!|\?|\.|"|:|\$/g, "");
    var monPrix = monAlbum.prix;
    var idAlbum = monAlbum.idSerie;
    var maSerie = series.get(idAlbum).nom;
    maSerie = maSerie.replace(/'|!|\?|\.|"|:|\$/g, "");
    var monNumero = monAlbum.numero;
    //test si prix incorrect :
    if (isNaN(parseFloat(monAlbum.prix))) {
        monPrix = "Prix à définir";
    }

    var monImage = "albumsMini/" + maSerie + "-" + monNumero + "-" + nomAlbum + ".jpg";

    cartInsesStor(nomAlbum, monPrix, monImage, idAlbum, monNumero);

    sum = total();

    afficherPanier();

}
/**
 * construit et affiche le panier dans un modal
 */
function afficherPanier() {
    shopCart = JSON.parse(sessionStorage.getItem('shopCart'));
    if (shopCart == null || shopCart.length == 0) { alert("Votre panier est vide") }

    else {
        var cart = document.getElementById("myBd");
        cart.innerHTML = "";
        for (let i = 0; i < shopCart.length; i++) {
            var image = (shopCart[ i ][ 'image' ]);
            var prx = (shopCart[ i ][ 'prix' ]);
            var album = (shopCart[ i ][ 'nom' ]);
            var idAlbum = (shopCart[ i ][ 'id' ]);
            var btnPaiment = document.getElementById("topaiment");
            btnPaiment.addEventListener("click", function () { location.href = "paiment.html" });
            prx = new Intl.NumberFormat('fr-FR', { style: 'currency', currency: 'EUR' }).format(parseFloat(prx / 100));

            var div = document.createElement("div");
            div.setAttribute("class", "ajoutitem");
            var img = document.createElement("img");
            img.setAttribute("src", image);
            img.setAttribute("class", "imgCart");
            var info = document.createElement("div");
            info.setAttribute("class", "speech");
            info.innerHTML = album + "<br>" + prx + "<br>";



            btnSupp = document.createElement("INPUT");
            info.appendChild(btnSupp);
            div.appendChild(img);
            div.appendChild(info);
            cart.appendChild(div);


            div.setAttribute("id", "div" + idAlbum);
            btnSupp.setAttribute("type", "button");
            btnSupp.setAttribute("value", "Supprimer");
            btnSupp.setAttribute("id", idAlbum)
            btnSupp.addEventListener("click", function () { suppAlbum(shopCart, this) });
        }
        sum = total();

        display();
    }
}
/**
 * mets les infos titre et prix dans le local storage
 * @param {string} nomDeLalbum 
 * @param {number} prixDeLalbum 
 */
function cartInsesStor(nomDeLalbum, prixDeLalbum, cheminImage, idDeLalbum, numeroDeLalbum) {
    var infoCart = {
        'nom': nomDeLalbum, 'prix': parseInt((prixDeLalbum * 100)),
        'image': cheminImage, 'id': idDeLalbum, 'num': numeroDeLalbum
    };

    shopCart = [];

    if (JSON.parse(sessionStorage.getItem('shopCart')) == null) {
        shopCart.push(infoCart);
    } else {
        var bTest = true;
        shopCart = JSON.parse(sessionStorage.getItem('shopCart'));
        for (let i = 0; i < shopCart.length; i++) {
            if (shopCart[ i ][ 'id' ] == idDeLalbum.toString()
                && shopCart[ i ][ 'num' ] == numeroDeLalbum.toString()) {

                bTest = false
                alert("Vous avez déjà cet album !");
                break;

            }

        }
        if (bTest) {
            shopCart.push(infoCart);
        }
    };


    sessionStorage.setItem('shopCart', JSON.stringify(shopCart));
}

/**
 *  supprime un album du panier
 * 
 * @param {Array} shopCart tableau des achat
 * @param {} element le bouton supprimer sur lequel on click
 */
function suppAlbum(shopCart, element) {

    var idAsupp = element.id;
    var suppDiv = document.getElementById("div" + idAsupp);
    suppDiv.remove();

    for (let i = 0; i < shopCart.length; i++) {
        if (shopCart[ i ][ 'id' ] == idAsupp.toString()) {

            shopCart.splice(i, 1);
            sessionStorage.setItem('shopCart', JSON.stringify(shopCart));

        }
        total();


    }

}

/**
 * 
 * @returns somme total des achats
 */
function total() {
    var sum = 0;
    cartShop = JSON.parse(sessionStorage.getItem('shopCart'));


    for (let i = 0; i < cartShop.length; i++) {

        sum += (cartShop[ i ][ 'prix' ]);

    }
    affichageDuPrixTotale(sum);
    return sum;
}

/**
 * affiche le prix total dans le panier
 * 
 * @param {number} sum 
 */
function affichageDuPrixTotale(sum) {
    var btnPaiment = document.getElementById("lePrixTotale");

    btnPaiment.innerHTML = "Total : " + new Intl.NumberFormat('fr-FR', { style: 'currency', currency: 'EUR' }).format(parseFloat(sum / 100));

}




