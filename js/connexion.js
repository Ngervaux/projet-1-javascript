var btnlog = document.getElementById("log");
var btnenter = document.getElementById("enter");
var btnexit = document.getElementById("exit");
var formLog = document.getElementById("formulaireLog")
var controleNom = /([^a-zA-Zâêîôûàèùòÿäëïöüÿéãñõ])/ //Liste symboles acceptés
var controleMotDePasse = /([^a-zA-Z0-9âêîôûàèùòÿäëïöüÿéãñõ!-_])/


formLog.addEventListener("keyup", testbouton)
btnlog.addEventListener("click", controleChamp);
btnenter.addEventListener("click", afficherConnexion);
btnexit.addEventListener("click", deconnecter);


//Test si touche entrée :
function testbouton(k) {
    if (k.keyCode === 13) {
        controleChamp();
    }
}

//Contrôle des champs du login :
function controleChamp() {
    var barreUserName = document.getElementById("username").value;
    var barrePassword = document.getElementById("password").value;

    if (barreUserName == "" || barrePassword == "") {
        alert("Un des champs est vide");
    }
    else if (controleNom.test(barreUserName) || controleMotDePasse.test(barrePassword)) {
        alert("Caractère invalide présent dans la saisie");
    }
    else {
        testlog();
    }

}

//Test si précence couple ID dans la map
function testlog() {
    var barreUserName = document.getElementById("username").value;
    var barrePassword = document.getElementById("password").value;
    var btest = false;
    for (let i = 1; i <= identifiants.size; i++) {

        var userName = identifiants.get(i).nomUtilisateur;
        var password = identifiants.get(i).motDePasse;
        if (userName == barreUserName && password == barrePassword) {
            btest = true;
            localStorage.setItem('Idconnexion', identifiants.get(i).id);
            var divModal = document.getElementById("formulaireLog");
            divModal.style.display = "none";
            btnenter.style.display = "none";
            btnexit.style.display = "block";
            afficheMessage(btest, i);

        }
        else if (userName == barreUserName && password != barrePassword) {
            btest = false;

        }

    }
    if (!btest) {
        var divModal = document.getElementById("formulaireLog");
        divModal.style.display = "none";
        afficheMessage(btest);
    }
}

function afficherConnexion() {

    var divModal = document.getElementById("formulaireLog");
    divModal.style.display = "inline";





    window.onclick = function (event) {
        if (event.target == divModal) {
            divModal.style.display = "none";
        }
    }

}

function afficheMessage(bool, indice) {
    var divInfos = document.getElementById("messageInfo");
    var dataInfos = document.getElementById("dataInfo");
    if (bool) {
        dataInfos.innerHTML = "";
        dataInfos.innerHTML = "Bienvenue " + identifiants.get(indice).nom + " " + identifiants.get(indice).prenom;
        divInfos.style.display = "inline";


    }

    else {
        dataInfos.innerHTML = "";
        dataInfos.innerHTML = "Erreur dans le nom d'utilisateur ou le mot de passe";
        divInfos.style.display = "inline";

    }
    window.onclick = function (event) {
        if (event.target == divInfos) {
            divInfos.style.display = "none";
            if (!bool) {
                afficherConnexion();
            }
        }
    }

}

function deconnecter() {
    var divInfos = document.getElementById("messageInfo");
    var dataInfos = document.getElementById("dataInfo");
    btnenter.style.display = "block";
    btnexit.style.display = "none";
    dataInfos.innerHTML = "";
    dataInfos.innerHTML = "Aurevoir et à bientôt !"
    divInfos.style.display = "inline";


    localStorage.clear();
    window.onclick = function (event) {
        if (event.target == divInfos) {
            divInfos.style.display = "none";

        }

    }
}
